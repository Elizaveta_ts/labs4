/*���������, ������� ����������� ���������� ������������� � �����,
� ����� ��������� ������ ��� ������������ � ��� �������. ��������� ������
���������� ������ �������� � ������ ������� ������������*/
#include <stdio.h>
#include <string.h>

#define NUMBER 25 //���� �� ������������� ����� ������
#define NAME 100

int main()
{
	char buf[NUMBER][NAME];
	int i = 0, count = 0, young = 100, old = 0, age = 0; //������, ���-�� �������������,���������� ��� ���������
	char *younger;
	char *elder;


	puts("Please, enter count of relatives");
	scanf("%d", &count);
	
	for (i = 0; i < count; i++)
	{
		printf("Please, enter name of relative number %d\n",i+1);
		scanf("%s", buf[i]);
		printf("Enter age of %s\n",buf[i]);
		scanf("%d", &age);

		if (age > old) //���� �������� ������� ������ ������ �������� ���������� ��������,��
		{
			old = age; //����������� ������ �������� ����� �������
			elder = buf[i]; //������� ���������
		}
		if (age < young) //���� �������� ������� ������ ������ �������� ���������� ��������
		{
			young = age; //����������� ������ �������� ����� �������
			younger = buf[i]; //������� ��������� 
		}
	}
	printf("Younger relative is %s, %d years old. Elder relative is %s, %d years old\n",younger, young, elder, old);

	return 0;
}