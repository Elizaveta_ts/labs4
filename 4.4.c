#include <stdio.h>
#include <string.h>

int main()
{
	char buf[256];
	int i = 0, count = 0;
	int max = 0, sequence = 0;
	char *begin, *end;
	printf("Enter the string, please: \n");
	fgets(buf, 256, stdin);
	buf[strlen(buf) - 1] = 0;

	while (buf[i])
	{
		if (buf[i] == buf[i + 1])  // sequence started
		{
			sequence = 1;
			count++;
		}
		if (buf[i] != buf[i + 1] && sequence == 1) //sequence finished
		{
			count++;
			if (max < count)
			{
				max = count; //set into memory number of letters
				begin = &buf[i - count + 1]; //set into memory pozition of first letter in sequence
				end = &buf[i];
			}
			sequence = 0;
			count = 0;
		}
		i++;
	}
	printf("number of letters in maximal sequence:%d, maximal sequence: ", max); //print number of maximal sequence
	while (begin < end)
		putchar(*begin++);
	putchar('\n');
	return 0;
}