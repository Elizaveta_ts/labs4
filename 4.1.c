/* ��������� ������ � ��������� �� � ������� �����������*/
#include <stdio.h>
#define SIZE 80 // ���������� ����� ������, �������� \0 
#define LIM 20 // ������������ ���������� ����������� ����� 
#define HALT   "" // ������� ������ ��� ����������� ����� 

/* ������� ���������� ����� � ������������e� ������e�e� */
void strsort(char *strings[], int num)
{
	char *temp;
	int top, seek;
	for (top = 0; top < num - 1; top++)
		for (seek = top + 1; seek < num; seek++)
			if (strlen(strings[top]) > strlen(strings[seek]))
			{
				temp = strings[top];
				strings[top] = strings[seek];
				strings[seek] = temp;
			}
}

void main()
{
	static char input[LIM][SIZE+1]; // ������ ��� ����������� �������� ����� 
	char *ptstr[LIM];  // ������ ���������� ���� ��������� 
	int ct = 0; // ������� �������� ����� 
	int k;  // ������� ��������� ����� 
	printf("Enter maximum %d number of strings and I will sort it.\n", LIM);
	printf("To finish put empty string.\n");
	while (fgets(input[ct], SIZE, stdin) != NULL)
	{
		if (input[ct][strlen(input[ct]) - 1] == '\n') input[ct][strlen(input[ct]) - 1] = '\0';
		if (strcmp(input[ct], HALT) == 0) break;
		ptstr[ct] = input[ct];  //��������� �� ��� ����������������� ���� 
		if (ct++ == LIM) break;
	}
	strsort(ptstr, ct); // ���������� ����� 
	puts(" \nIt's a sorted list of strings:\n");
	for (k = 0; k < ct; k++)
		puts(ptstr[k]); // ��������� �� ��������������� ������ 
	getch();
}

